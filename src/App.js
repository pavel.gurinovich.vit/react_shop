import { BrowserRouter, Route } from "react-router-dom";
import Header from './components/Header/Header'
import Home from './components/Home/Home'
import AdminPart from "./components/Admin/AdminPart";
import UserPart from "./components/User/UserPart";
import Profile from "./components/Profile/Profile";
import style from './App.module.css';
import HOC from "./components/hoc/hoc"


const App = () =>{
    return (
        <BrowserRouter> 
                <div className={style.App}>
                    <Header/>
                    <Route path={'/home'} render={()=><Home/>}/>
                    <Route path={'/admin'} render={()=><AdminPart/>}/>
                    <Route path={'/user'} render={()=><UserPart/>}/>
                    <HOC exact path={'/profile'} component={Profile}/>
                </div>
        </BrowserRouter>
    );
}


export default App;
