import {FIND_PRODUCT} from "../constants/product.constants";

export const initialFilterState = ''

export const filterProductsReducer = (state = initialFilterState, action)=>{
    switch(action.type){
        case FIND_PRODUCT:
            return action.data?action.data: ""
        default: return state;
    }
}