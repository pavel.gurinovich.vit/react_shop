import {CREATE_PRODUCT, DELETE_PRODUCT, EDIT_PRODUCT} from "../constants/product.constants";

export const initialState = [
    {id: 1, name:'Milk', img: `https://cdn.pixabay.com/photo/2016/12/06/18/27/cereal-1887237_960_720.jpg`},
    {id: 2, name:'Bread', img: `https://cdn.pixabay.com/photo/2018/06/19/09/34/bread-3484107_960_720.jpg`}
];


export const productReducer = (state = initialState, action)=>{
    switch(action.type){
        case CREATE_PRODUCT:
            return [...state, action.data]
        case EDIT_PRODUCT:
            let newState = []
            state.forEach(el=>{
                if(el.id === action.data.id){
                    newState.push(action.data)
                }else{
                    newState.push(el)
                }
            });
            return newState
        case DELETE_PRODUCT: return state.filter(el => el.id !== action.id)
        default: return state;
    }
}