import {EDIT_PROFILE} from "../constants/profile.constants";

export const initialProfileState = {
    name: "Pasha",
    surname: "pasha@gmial.com",
    card: 1234567890123456
}

export const profileReducer = (state = initialProfileState, action)=>{
    switch(action.type) {
        case EDIT_PROFILE:
            return action.data
        default:
            return state;
    }
}