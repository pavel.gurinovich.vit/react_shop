import {SET_USERS} from "../constants/user.constants";

const initialState = [{}]

export const userReducer = (state = initialState, action)=>{
    switch(action.type){
        case SET_USERS: return action.data
        default: return state
    }
}   