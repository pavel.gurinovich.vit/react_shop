import { AUTHENTICATE } from "../constants/user.constants";

export const defaultAuthState = {isAuth: true};

export const authReducer = (state = defaultAuthState, action) => {
    switch (action.type) {
        case AUTHENTICATE: return {isAuth: !state.isAuth}
        default:
            return state;
    }
};
