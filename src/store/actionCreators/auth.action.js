import {  AUTHENTICATE } from "../constants/user.constants";

export const authenticate = () => ({type: AUTHENTICATE})