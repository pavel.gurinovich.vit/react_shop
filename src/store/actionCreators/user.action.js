import {LOAD_USERS, SET_USERS} from "../constants/user.constants";

export const setUsers = (data) => ({type: SET_USERS, data})
export const loadUsers = () =>({type: LOAD_USERS})