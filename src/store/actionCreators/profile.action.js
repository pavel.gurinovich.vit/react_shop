import {EDIT_PROFILE} from "../constants/profile.constants";

export const editProfile = (data) => ({type:EDIT_PROFILE, data})