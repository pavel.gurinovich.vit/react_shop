import {CREATE_PRODUCT, DELETE_PRODUCT, EDIT_PRODUCT, FIND_PRODUCT} from "../constants/product.constants";

export const createProduct = (data) => ({type: CREATE_PRODUCT, data});
export const editProduct = (data) => ({type: EDIT_PRODUCT, data});
export const deleteProduct = (id) => ({type: DELETE_PRODUCT, id});
export const onFindProduct = (data) =>({type: FIND_PRODUCT, data})