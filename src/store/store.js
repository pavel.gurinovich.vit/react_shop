import {applyMiddleware, combineReducers, createStore} from "redux";
import { filterProductsReducer } from "./reducers/filter.reducer";
import { homeReducer } from "./reducers/home.reducer";
import {productReducer} from "./reducers/product.reducer";
import {profileReducer} from "./reducers/profile.reducer";
import { userReducer } from "./reducers/user.reducer";
import { authReducer } from "./reducers/auth.reducer";
import rootSaga from "./sagas/rootSaga";
import createSagaMiddleware from "@redux-saga/core";

const sagaMiddleware = createSagaMiddleware()

const rootReducer  = combineReducers({
    products: productReducer,
    profile: profileReducer,
    filter: filterProductsReducer,
    home: homeReducer,
    users: userReducer,
    auth: authReducer
})


export const store = createStore(rootReducer,  applyMiddleware(sagaMiddleware))
sagaMiddleware.run(rootSaga)