import { takeLatest, call, put } from "redux-saga/effects";
import { LOAD_USERS } from "../constants/user.constants";
import { getUsers } from "../api/user.api";
import { setUsers } from "../actionCreators/user.action";


export function* loadUsersSaga (){
    yield takeLatest(LOAD_USERS, loadUsersFlow)
}

function* loadUsersFlow(){
    const users = yield call(getUsers)
    yield put(setUsers(users))
}