import {fork} from 'redux-saga/effects'
import {loadUsersSaga} from './loadUsersSaga'
export default function* rootSaga() {
   yield fork(loadUsersSaga)  
}