import React from 'react';
import {connect} from "react-redux";
import style from './Home.module.css';

const Home = (props) =>{

    return (
        <div className="home">
           <h1 className={style.title}>{props.home.title}</h1>
           <img className={style.img} src={props.home.img} alt={props.home.title}/>
           <p className={style.data}>{props.home.data}</p>
        </div>
    );
}

const mapStateToProps = (state) => ({
    home: state.home
})

export default connect(mapStateToProps)(Home)
