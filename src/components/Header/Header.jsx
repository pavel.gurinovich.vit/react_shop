import {NavLink} from "react-router-dom";
import Auth from "./Auth";
import style from './Header.module.css';


const Header = () =>{
    return (
        <div className={style.header}>
            <div className={style.linkContainer}><NavLink className={style.link} to='/home'>Home</NavLink></div>
            <div className={style.linkContainer}><NavLink className={style.link} to='/admin'>Admin</NavLink></div>
            <div className={style.linkContainer}><NavLink className={style.link} to='/user'>User</NavLink></div>
            <div className={style.linkContainer}><NavLink className={style.link} to='/profile'>Profile</NavLink></div>
            <div className={style.linkContainer}><Auth/></div>
        </div>
    );
}

export default Header;
