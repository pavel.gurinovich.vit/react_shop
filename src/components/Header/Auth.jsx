import style from './Header.module.css';
import { authenticate } from "../../store/actionCreators/auth.action";
import { connect } from "react-redux";

const Auth = (props) =>{
    const handleChange = () => {
        props.authenticate()
    }
    return (
        <div className={style.header}>
           <button onClick={handleChange}>{props.auth.isAuth ? "Logout" : "Login"}</button>
        </div>
    );
}

const mapStateToProps = (state) => ({
    auth: state.auth
})


export default connect(mapStateToProps, {authenticate})(Auth);
