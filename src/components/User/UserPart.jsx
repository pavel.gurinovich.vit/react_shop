import React, {useState} from "react";
import style from '../Admin/Admin.module.css';
import {connect} from "react-redux";
import {onFindProduct} from "../../store/actionCreators/product.action";

const UserPart = (props) =>{
    
    const [name, setName] = useState('')

    const findProduct = () => {
        props.onFindProduct(name)
    }
    
    return (
        <div className="user">
            <div className={style.inputContent}>
                <h2>Поиск товара:</h2>
                <input placeholder={'Например: молоко'} type="text" value={name} onChange={(e)=>setName(e.target.value)}/>
                <button className={style.button}ds onClick={findProduct}>Найти продукт</button>
            </div>
            {
                props.products.map(product => (
                    <div className={style.contentContainer} key={product.id}>
                        <div className={style.title} >{product.name}</div>
                        <img className={style.img} src={product.img} alt={product.id}/>
                    </div>
                ))
            }
        </div>
    );
}

const mapStateToProps = (state) => ({
    products: state.products.filter(product => product.name.toLowerCase().includes(state.filter.toLowerCase()))
})


export default connect(mapStateToProps, {onFindProduct})(UserPart);
