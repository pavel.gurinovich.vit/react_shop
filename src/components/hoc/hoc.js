import React from 'react';
import { connect} from "react-redux";
import { Route } from 'react-router-dom';


const Login  = () =>{
    return (
        <div className={'Login'}>
             <input type="text" placeholder={'Email'}/>
             <input type="password" placeholder={'Password'}/> 
             <button>Login</button>
        </div>
    )
}

const HOC = ({component: Component, auth, ...rest}) =>(
    <Route 
        {...rest}
        render={props => {
        
            return(
                auth.isAuth ? (
                    <Component {...props}/>
                ):(<Login/>)
            );
        }}
    />
)

const mapStateToProps = (state) => ({
    auth: state.auth
});

export default connect(mapStateToProps)(HOC);