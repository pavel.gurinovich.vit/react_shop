import React from "react";

const ErrorHandler = (props) =>{
    
    const showError = ()=>{ 
        switch(props.length){   
            case 15: return 'Значение меньше необходимого'
            case 16: return 'Значение введено корректно'
            case 17: return 'Значение больше необходимого'
            default: return ''
        }
    }

    return (
        <div className="Error">
           {showError()}
        </div>
    );
}

export default React.memo(ErrorHandler, (prevProps, nextProps)=>{
    if(nextProps.length >= 15 && nextProps.length <= 17){
        return false
    }else if(prevProps.length>=15 && prevProps.length<=17){
        return false
    }else{
        return true
    }
})
