import React, {useState} from "react";
import style from './Profile.module.css';
import Input from "./Input";
import {editProfile} from "../../store/actionCreators/profile.action";
import {connect} from "react-redux";

const Profile = (props) =>{
    const [name, setName] = useState('')
    const [surname, setSurname] = useState('')
    const [card, setCard] = useState('')
    
    const handleChange = () => {
        let profile = {

            name: name ? name : props.profile.name,
            surname: surname ? surname : props.profile.surname,
            card: card.length !== 16 ? props.profile.card: card
        }

        props.editProfile(profile)

        setName('')
        setSurname('')
        setCard('') 
    }

    return (
        <div className="profile">
            <p className={style.data}>Name: {props.profile.name}</p>
            <p className={style.data}>Surname: {props.profile.surname}</p>
            <p className={style.data}>Card: {props.profile.card}</p>
            <h2>Изменить данные о пользователе:</h2>

            <Input data={name} setData={setName} placeholder={'Name'}/>
            <Input data={surname} setData={setSurname } placeholder={'Surname'} />
            <Input data={card} setData={setCard} placeholder={'Card'} flag={true}/>
       
            <button className={style.button} onClick={handleChange}>Подтвердить</button>
        </div>
    );
}

const mapStateToProps = (state) => ({
    profile: state.profile
})


export default connect(mapStateToProps, {editProfile})(Profile)
