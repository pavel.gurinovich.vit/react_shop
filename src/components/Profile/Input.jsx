import React from "react";
import ErrorHandler from "./ErrorHandler";


const Input = (props) =>{
    return (
        <div className="input">
            <input type="text" placeholder={props.placeholder} value={props.data} onChange={(e)=>props.setData(() => e.target.value)}/>
            {props.flag && <ErrorHandler length={props.data.length}/>}
        </div>
    );
}

export default React.memo(Input)