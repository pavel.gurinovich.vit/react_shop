import React,{ useState} from 'react';
import style from './Admin.module.css';
import {connect} from "react-redux";
import {createProduct, editProduct, deleteProduct} from "../../store/actionCreators/product.action";
import { productFilter } from '../../store/selectors/product.selector';
import { loadUsers } from '../../store/actionCreators/user.action';
import { useEffect } from 'react';
const AdminPart = (props) =>{

    const [name, setName] = useState('');   
    const [id, setId] = useState();
    const [isEdit, setIsEdit] = useState(false);
    const [img, setImg] = useState('')
    const [flag, setFlag] = useState(false)
    const onUsersList = () =>{
        setFlag(!flag)
       
    }

    useEffect(()=>{
        console.log('render')
        props.loadUsers()
    }, [])

    const createNewProduct = () =>{
        let product = { 
            id: isEdit ? id :  props.products.length !== 0 ?  props.products[ props.products.length-1].id + 1 : 1,
            name: name,
            img: img ? img : 'https://zastavok.net/main/priroda/162365869688.jpg'
        };

        if(name) {
            if (isEdit) {
                props.editProduct(product)
            } else {
                props.createProduct(product)
            }
        }

        setName('')
        setIsEdit(false)
    }

    const handleEdit = (product) => {
        setIsEdit(true);
        setId(product.id)
        setName(product.name);
    }

    const handleDelete = (product) => {
        props.deleteProduct(product.id)
    }

    return (
        <div className="admin">
            <div className={style.inputContent}>
                <h2>Добавить товар:</h2>
                <input type="text" placeholder={'Product'} value={name} onChange={(e)=>setName(e.target.value)}/>
                <input type="text" placeholder={'Link'} value={img} onChange={(e)=>setImg(e.target.value)}/>
                <button  className={style.button} onClick={createNewProduct}>Подтвердить</button>
            </div>
            <button  className={style.button} onClick={onUsersList}>{flag?"Вывести продукты":"Вывести список пользователей"}</button>
            {!flag?props.products.map(product => (
                    <div className={style.contentContainer} key={product.id}>
                        <div className={style.title}>{product.name}</div>
                        <div ><img className={style.img} src={product.img} alt={product.id}/></div>
                        
                        <div>
                            <button className={style.button} onClick={() => handleEdit(product)}>Изменить</button>
                            <button className={style.button} onClick={() => handleDelete(product)}>Удалить</button>
                        </div>
                    </div>
                )):
                props.users.map(user=>(
                    <ul>
                        <li>
                        {user.name}
                        </li>
                    </ul>

                ))

            }
            {/* {
                
            }
            {
                props.users.map(user=>(
                    <div>{user.name}</div>
                ))
            } */}

        </div>
    );
}

const mapStateToProps = (state) => ({
    products: state.products,
    users: state.users
    
})



export default connect(mapStateToProps, {createProduct, editProduct, deleteProduct, loadUsers})(AdminPart);
